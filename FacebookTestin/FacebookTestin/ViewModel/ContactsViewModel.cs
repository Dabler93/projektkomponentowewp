﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;
using Cimbalino.Toolkit.Services;
using FacebookTestin.Services.Interfaces;
using FacebookTestin.Services.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json.Linq;

namespace FacebookTestin.ViewModel
{
    public class ContactsViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private readonly INavigationService _navigationService;
        private readonly ILogManager _logManager;
        private readonly ISessionService _sessionService;
        private readonly IMessageBoxService _messageBox;
        private UserData userData;
        private Boolean navigatedTo = false;
        private String userId;

        private MobileServiceCollection<Friend, Friend> items;
        private IMobileServiceTable<Friend> friendsTable = App.MobileService.GetTable<Friend>();

        public ObservableCollection<Friend> FriendsList { get; set; }


        public ContactsViewModel(INavigationService navigationService, 
            ILogManager logManager,
            IMessageBoxService messageBox,
            ISessionService sessionService)
        {
            _navigationService = navigationService;
            _logManager = logManager;
            _sessionService = sessionService;
            _messageBox = messageBox;
            navigationService.Navigated += OnNavigated;
            FriendsList = new ObservableCollection<Friend>();
            userId = _sessionService.GetUserId();

            GoToChat = new RelayCommand(
            async () =>
            {
                
            });
        }

        private void OnNavigated(object sender, EventArgs e)
        {
            if (!navigatedTo)
            {
                userData = (UserData) _navigationService.CurrentParameter;
                navigatedTo = false;
                RefreshFriendsList();
            }
        }

        public ICommand GoToChat { get; private set; }

        private async Task RefreshFriendsList()
        {
            List<Friend> list = new List<Friend>();
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("user", userId);
            var results = await App.MobileService.InvokeApiAsync("CustomContacts/Post", HttpMethod.Post, parameters);
            JArray array = results.Value<JArray>();
            foreach (JObject item in array)
            {
                Friend friend = new Friend();
                friend.id = item["id"].Value<String>();
                friend.SecondId = item["target"].Value<String>();
                friend.SecondsName = item["targetName"].Value<String>();
                list.Add(friend);
                FriendsList.Add(friend);
            }
            NotifyPropertyChanged("FriendsList");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
