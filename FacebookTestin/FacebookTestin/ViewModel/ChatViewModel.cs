﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;
using Windows.UI.Xaml.Media.Imaging;
using Cimbalino.Toolkit.Services;
using FacebookTestin.Services.Interfaces;
using FacebookTestin.Services.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json.Linq;

namespace FacebookTestin.ViewModel
{
    public class ChatViewModel: ViewModelBase, INotifyPropertyChanged
    {
        private readonly INavigationService _navigationService;
        private readonly ILogManager _logManager;
        private readonly ISessionService _sessionService;
        public ObservableCollection<Message> Messages { get; set; }
        private String userId;

        public ChatViewModel(INavigationService navigationService, 
            ILogManager logManager, 
            ISessionService sessionService)
        {
            _navigationService = navigationService;
            _logManager = logManager;
            _sessionService = sessionService;
            Messages = new ObservableCollection<Message>();
            navigationService.Navigated += OnNavigated;
            userId = _sessionService.GetUserId();
            GoToContacts = new RelayCommand(
                async () =>
                {
                    _navigationService.Navigate<Contacs>();    
                });
        }
        
        public ICommand GoToContacts { get; private set; }
        
        private async void OnNavigated(object sender, EventArgs e)
        {
//            getAllMessages(userId);
        }
        private async void getAllMessages(String userId, String targetId)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("sourceUser", userId);
            parameters.Add("targetUser", targetId);
            var results = await App.MobileService.InvokeApiAsync("CustomChat/Post", HttpMethod.Post, parameters);
            JArray array = results.Value<JArray>();
            foreach (JObject item in array)
            {
                Message message = new Message();
                message.UpdatedAt = item["updatedAt"].Value<DateTime>();
                message.SenderId = item["senderId"].Value<String>();
                message.MessageText = item["message"].Value<String>();
                Messages.Add(message);
            }
         }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
