﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Cimbalino.Toolkit.Extensions;
using Cimbalino.Toolkit.Services;
using Facebook;
using FacebookTestin.ServerCommunicationModels;
using FacebookTestin.Services.Interfaces;
using FacebookTestin.Services.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json.Linq;

namespace FacebookTestin.ViewModel
{
    public class RegisterPreferencesViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private readonly INavigationService _navigationService;
        private readonly ILogManager _logManager;
        private readonly ISessionService _sessionService;
        private readonly IMessageBoxService _messageBox;
        private UserData userData;
        private Boolean navigatedTo = false;

        public RegisterPreferencesViewModel(INavigationService navigationService, 
            ILogManager logManager,
            IMessageBoxService messageBox,
            ISessionService sessionService)
        {
            _navigationService = navigationService;
            _logManager = logManager;
            _sessionService = sessionService;
            _messageBox = messageBox;
            navigationService.Navigated += OnNavigated;

            RegisterOnServer = new RelayCommand(
                () =>
                {
                    int age = UsersPreferredAgeText;
                    string city = UsersPrefferedCityText;
                    Boolean womanPreferred = userData.gender.Equals("female") ? false : true;
                    userData = UpdateUsersPreferences(userData, age, city, womanPreferred);
                    SendToServer(userData);
                });

        }

        private void OnNavigated(object sender, EventArgs e)
        {
            if (!navigatedTo)
            {
                userData = (UserData) _navigationService.CurrentParameter;
                navigatedTo = false;
            }
        }

        public int UsersPreferredAgeText { get; set; }

        public String UsersPrefferedCityText { get; set; }

        public ICommand RegisterOnServer { get; private set; }

        private UserData UpdateUsersPreferences(UserData userData, int preferredAge, String preferredCity, Boolean isWomanPreferred)
        {
            userData.preferredAge = preferredAge;
            userData.preferredCity = preferredCity;
            userData.womansPrefferedGender = isWomanPreferred;
            return userData;
        }

        private async void SendToServer(UserData userData)
        {
            var myOrder = new JObject();
            myOrder.Add("access_token", userData.access_token);
            myOrder.Add("firstName", userData.firstName);
            myOrder.Add("lastName", userData.lastName);
            myOrder.Add("gender", userData.gender);
            myOrder.Add("email", userData.email);
            myOrder.Add("picture", userData.picture);
            myOrder.Add("age", userData.Age);
            myOrder.Add("city", userData.City);
            myOrder.Add("preferredCity", userData.preferredCity);
            myOrder.Add("preferredAge", userData.preferredAge);
            myOrder.Add("womansPrefferedGender", userData.womansPrefferedGender);
//            try
//            {
                var result = await
        App.MobileService.InvokeApiAsync("CustomRegistration/Post", myOrder);

                _navigationService.Navigate<MainPage>();
//            }
//            catch (Exception e)
//            {
//                String ex = e.Message;
//                await _messageBox.ShowAsync("There isn´t network connection.",
//                                          "Authentication Sample",
//                                          new List<string> { "Ok" });
//            }
        }

    }
}
