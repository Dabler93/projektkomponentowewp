﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LoginViewModel.cs" company="saramgsilva">
//   Copyright (c) 2013 saramgsilva. All rights reserved. 
// </copyright>
// <summary>
//   The login view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Data.Json;
using Windows.Web.Http;
using FacebookTestin.Services.Interfaces;
using Cimbalino.Toolkit.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Windows.ApplicationModel.Activation;
using FacebookTestin;
using Newtonsoft.Json.Linq;

namespace FacebookTestin.ViewModel
{
    /// <summary>
    /// The login view model.
    /// </summary>
    public class LoginViewModel : ViewModelBase
    {
        private readonly ILogManager _logManager;
        private readonly IMessageBoxService _messageBox;
        private readonly INetworkInformationService _networkInformationService;
        private readonly INavigationService _navigationService;
        private readonly ISessionService _sessionService;
        private bool _inProgress;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginViewModel"/> class.
        /// </summary>
        /// <param name="navigationService">
        /// The navigation service.
        /// </param>
        /// <param name="sessionService">
        /// The session service.
        /// </param>
        /// <param name="messageBox">
        /// The message box.
        /// </param>
        /// <param name="networkInformationService">
        /// The network connection.
        /// </param>
        /// <param name="logManager">
        /// The log manager.
        /// </param>
        public LoginViewModel(INavigationService navigationService,
            ISessionService sessionService,
            IMessageBoxService messageBox,
            INetworkInformationService networkInformationService,
            ILogManager logManager)
        {
            _navigationService = navigationService;
            _sessionService = sessionService;
            _messageBox = messageBox;
            _networkInformationService = networkInformationService;

            _logManager = logManager;
            LoginCommand = new RelayCommand<string>(LoginAction);
        }

        /// <summary>
        /// Gets the navigation service.
        /// </summary>
        /// <value>
        /// The navigation service.
        /// </value>
        public INavigationService NavigationService
        {
            get { return _navigationService; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether in progress.
        /// </summary>
        /// <value>
        /// The in progress.
        /// </value>
        public bool InProgress
        {
            get { return _inProgress; }
            set { Set(() => InProgress, ref _inProgress, value); }
        }

        /// <summary>
        /// Gets the facebook login command.
        /// </summary>
        /// <value>
        /// The facebook login command.
        /// </value>
        public ICommand LoginCommand { get; private set; }

        /// <summary>
        /// Facebook's login action.
        /// </summary>
        /// <param name="provider">
        /// The provider.
        /// </param>
        private async void LoginAction(string provider)
        {
            Exception exception = null;
            bool isToShowMessage = false;
            try
            {
                if (!_networkInformationService.IsNetworkAvailable)
                {
                    await _messageBox.ShowAsync("There isn´t network connection.",
                                          "Authentication Sample",
                                          new List<string> { "Ok" });
                    return;
                }
                if (Constants.FacebookAppId.Contains("<"))
                {
                    await _messageBox.ShowAsync("Is missing the facebook client id. Search for Constant.cs file.",
                                         "Authentication Sample",
                                         new List<string> { "Ok" });
                    return;
                }
                InProgress = true;
                var auth = await _sessionService.LoginAsync(provider);
                if (auth == null)
                {
                    return;
                }
                if (!auth.Value)
                {
                    await ShowMessage();
                }
                else
                {
                    String accessToken = _sessionService.GetSession().AccessToken;
                    JObject myLoginDatas = new JObject("access_token", accessToken);
                    try
                    {
                        var result = await App.MobileService.InvokeApiAsync("CustomRegistration/Post", myLoginDatas);
                        _navigationService.Navigate<MainPage>();
                    }
                    catch (Exception)
                    {
                        _navigationService.Navigate<RegisterView>();
                    }
                    InProgress = false;
                }

                InProgress = false;
            }
            catch (Exception ex)
            {
                InProgress = false;
                exception = ex;
                isToShowMessage = true;
            }
            if (isToShowMessage)
            {
                await _messageBox.ShowAsync("Application fails.",
                                           "Authentication Sample", 
                                            new List<string> { "Ok" });
            }
            if (exception != null)
            {
                await _logManager.LogAsync(exception);
            }
        }

        private async Task ShowMessage()
        {
            await _messageBox.ShowAsync("Wasn´t possible to complete the login.",
               "Authentication Sample",
                new List<string>
                {
                   "Ok" 
                });
        }

#if WINDOWS_PHONE_APP
        public async void Finalize(WebAuthenticationBrokerContinuationEventArgs args)
        {
            var result = await _sessionService.Finalize(args);
            if (!result)
            {
                await ShowMessage();
            }
            else
            {
                String accessToken = _sessionService.GetSession().AccessToken;

                JObject myLoginDatas = new JObject();
                var httpClient = new HttpClient();
                var response = await httpClient.GetStringAsync(new Uri("https://graph.facebook.com/me?fields=id&access_token=" + accessToken));
                var value = JsonValue.Parse(response).GetObject();
                String facebookId = value.GetNamedString("id");
                _sessionService.SetUserId(facebookId);
                myLoginDatas.Add("access_token", facebookId);
                try
                {
                    var postResult = await App.MobileService.InvokeApiAsync("CustomLogin/Post", myLoginDatas);
                    _navigationService.Navigate<MainPage>();
                }
                catch (Exception)
                {
                    _navigationService.Navigate<RegisterView>();
                }
                InProgress = false;
            }
        }
#endif
    }
}
