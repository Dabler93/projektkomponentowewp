﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Data.Json;
using Windows.Web.Http;
using Cimbalino.Toolkit.Services;
using FacebookTestin.Services.Interfaces;
using FacebookTestin.Services.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace FacebookTestin.ViewModel
{
    public class RegisterViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly ILogManager _logManager;
        private readonly ISessionService _sessionService;
        private UserData userData;
        private Boolean navigatedTo = false;

        public RegisterViewModel(INavigationService navigationService, 
            ILogManager logManager, 
            ISessionService sessionService)
        {
            _navigationService = navigationService;
            _logManager = logManager;
            _sessionService = sessionService;
            userData = new UserData();
            navigationService.Navigated += OnNavigated;


            GetAgeAndCity = new RelayCommand(
                () =>
                {
                    int age = Int32.Parse(UsersAgeText);
                    string city = UsersCityText;
                    userData = GetUsersAgeAndCity(userData, age, city);
                    _navigationService.Navigate<RegisterPreferencesView>(userData);
                });

        }

        private void OnNavigated(object sender, EventArgs e)
        {
            if (!navigatedTo)
            {
                GetFacebookData(_sessionService, userData);
                navigatedTo = true;
            }
        }

        private async Task<UserData> GetFacebookData(ISessionService sessionService, UserData userData)
        {
            String accessToken = sessionService.GetSession().AccessToken;
            var httpClient = new HttpClient();
            var response = await httpClient.GetStringAsync(new Uri("https://graph.facebook.com/me?fields=first_name,last_name,gender,picture,email&access_token=" + accessToken));
            var value = JsonValue.Parse(response).GetObject();
            userData.firstName = value.GetNamedString("first_name");
            userData.lastName = value.GetNamedString("last_name");
            userData.gender = value.GetNamedString("gender");
            userData.email = value.GetNamedString("email");
            userData.access_token = value.GetNamedString("id");
            userData.picture = value.GetNamedObject("picture").GetNamedObject("data").GetNamedString("url");
            return userData;
        }

        private UserData GetUsersAgeAndCity(UserData userData, int age, String city)
        {
            userData.Age = age;
            userData.City = city;
            return userData;
        }

        public String UsersAgeText { get; set; }

        public String UsersCityText { get; set; }

        public ICommand GetAgeAndCity { get; private set; }

    }
}
