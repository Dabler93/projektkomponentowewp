// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainViewModel.cs" company="saramgsilva">
//   Copyright (c) 2013 saramgsilva. All rights reserved. 
// </copyright>
// <summary>
//   The main view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Data.Json;
using Windows.UI.Popups;
using FacebookTestin.Services.Interfaces;
using Cimbalino.Toolkit.Services;
using FacebookTestin.Services.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using FacebookTestin;
using Windows.UI.Xaml.Controls;
using Newtonsoft.Json.Linq;
using Windows.UI.Xaml.Media.Imaging;
using Cimbalino.Toolkit.Extensions;

namespace FacebookTestin.ViewModel
{
    /// <summary>
    /// The main view model.
    /// </summary>
    public class MainViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private readonly INavigationService _navigationService;
        private readonly ILogManager _logManager;
        private readonly ISessionService _sessionService;
        private List<PreferredUserResponse> preferredUsersList;
        private String userId;

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        /// <param name="navigationService">
        /// The navigation Service.
        /// </param>
        /// <param name="logManager">
        /// The log manager.
        /// </param>
        /// <param name="sessionService">The session service.</param>
        public MainViewModel(INavigationService navigationService, 
            ILogManager logManager, 
            ISessionService sessionService)
        {
            _navigationService = navigationService;
            _logManager = logManager;
            _sessionService = sessionService;
            preferredUsersList = new List<PreferredUserResponse>();
            navigationService.Navigated += OnNavigated;
            userId = _sessionService.GetUserId();
            LogoutCommand = new RelayCommand(
                () =>
                {
                    _sessionService.Logout();

                    // todo navigation
                    _navigationService.Navigate<LoginPage>();
                });
            AboutCommand = new RelayCommand(
                async () =>
                { 
                    Exception exception = null;
                    try
                    {
                        // todo navigation
                        // _navigationService.Navigate(new Uri(Constants.AboutView, UriKind.Relative));
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                    }

                    if (exception != null)
                    {
                        await _logManager.LogAsync(exception);
                    }
                });
            ILikeThisUser = new RelayCommand(
                async () =>
                {
                    RateUser(userId, preferredUsersList.First().FacebookId, true);
                });

            IDontLikeThisUser = new RelayCommand(
                async () =>
                {
                    RateUser(userId, preferredUsersList.First().FacebookId, false);    
                });
            GoToContacts = new RelayCommand(
                async () =>
                {
                    _navigationService.Navigate<Contacs>();    
                });
        }
        
        /// <summary>
        /// Gets the about command.
        /// </summary>
        /// <value>
        /// The about command.
        /// </value>
        public ICommand AboutCommand { get; private set; }
        public ICommand GoToContacts { get; private set; }

        private string exampleUserLocation;

        public String ExampleUserLocation
        {
            get {return exampleUserLocation;}
            set
            {
                exampleUserLocation = value;
                NotifyPropertyChanged("ExampleUserLocation");
            }
        }

        private string exampleUserAge;

        public String ExampleUserAge
        {
            get { return exampleUserAge;}
            set
            {
                exampleUserAge = value;
                NotifyPropertyChanged("ExampleUserAge");
            }
            
        }


        /// <summary>
        /// Gets the logout command.
        /// </summary>
        /// <value>
        /// The logout command.
        /// </value>
        public ICommand LogoutCommand { get; private set; }
        public ICommand ILikeThisUser { get; private set; }
        public ICommand IDontLikeThisUser { get; private set; }

        private BitmapImage profilePicture;

        public BitmapImage ProfilePicture
        {
            get { return profilePicture; }
            set
            {
                profilePicture = value;
                NotifyPropertyChanged("ProfilePicture");
            }
        }

        
        private async void OnNavigated(object sender, EventArgs e)
        {
            getPotentialPartners(userId);
        }
        private async void getPotentialPartners(String facebookId)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("user", facebookId);
            var results = await App.MobileService.InvokeApiAsync("PreferredUsers/Post", HttpMethod.Post, parameters);
            //            var value = JsonValue.Parse(results.ToString()).GetObject().GetArray();
            JArray array = results.Value<JArray>();
            foreach (JObject item in array)
            {
                PreferredUserResponse preferredUserResponse = new PreferredUserResponse();
                preferredUserResponse.FirstName = item["firstName"].Value<String>();
                preferredUserResponse.Gender = item["gender"].Value<String>();
                preferredUserResponse.FacebookId = item["accessToken"].Value<String>();
                preferredUserResponse.Age = item["age"].Value<String>();
                preferredUserResponse.Picture = item["picture"].Value<String>();
                preferredUserResponse.City = item["city"].Value<String>();
                preferredUsersList.Add(preferredUserResponse);
            }
            ShowNextUser(preferredUsersList);
        }

        private async Task<BitmapImage> DownloadImage(String id)
        {
            String uriString = "http://graph.facebook.com/"+id+"/picture?type=large";
            Uri myUri = new Uri(uriString, UriKind.Absolute);
            BitmapImage bmi = new BitmapImage();
            bmi.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
            bmi.UriSource = myUri;
            return bmi;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        private async void RateUser(String sourceUserId, String targetUserId, Boolean doLike)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("doLike", doLike.ToString());
            parameters.Add("sourceUser", sourceUserId);
            parameters.Add("targetUser", targetUserId);
            var results = await App.MobileService.InvokeApiAsync("Relation/Post", HttpMethod.Post, parameters);
            Boolean areFriends = results.Value<Boolean>();
            if (areFriends)
            {
                new MessageDialog("Failed to load the image").ShowAsync();
            } 
            preferredUsersList.RemoveAt(0);
            ShowNextUser(preferredUsersList);
        }

        private void ShowNextUser(List<PreferredUserResponse> preferredUserResponseList)
        {
            try
            {
                PreferredUserResponse nextUser = preferredUserResponseList.First();
                ExampleUserAge = nextUser.Age;
                ExampleUserLocation = nextUser.City;
                ProfilePicture = DownloadImage(nextUser.FacebookId).Result;
            }
            catch (Exception)
            {
                ExampleUserAge = "Brak nowych kandydatów";
                ExampleUserLocation = "";
                ProfilePicture = null;
            }

        }


    }
}