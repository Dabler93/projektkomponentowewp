﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookTestin.ServerCommunicationModels
{
    class RegisterCall
    {
        public String access_token;
        public String firstName;
        public String lastName;
        public String gender;
        public String email;
        public String picture;
        public String Age;
        public String City;
        public String preferredCity;
        public String preferredAge;
        public Boolean womansPrefferedGender;
    }
}
