﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookTestin.Services.Model
{
    public class PreferredUserResponse
    {
        public String FirstName;
        public String Gender;
        public String FacebookId;
        public String Age;
        public String Picture;
        public String City;

    }
}
