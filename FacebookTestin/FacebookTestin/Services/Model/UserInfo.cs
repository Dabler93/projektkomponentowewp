﻿
// ReSharper disable once CheckNamespace
namespace FacebookTestin.Services.Model
{
    /// <summary>
    /// The user info.
    /// </summary>
    public class UserInfo
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
    }
}
