﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookTestin.Services.Model
{
    class UserData
    {
        public string _ID;
        public String access_token;
        public String firstName;
        public String lastName;
        public String gender;
        public String email;
        public String picture;
        public int Age;
        public String City;
        public String preferredCity;
        public int preferredAge;
        public Boolean womansPrefferedGender;
    }
}
