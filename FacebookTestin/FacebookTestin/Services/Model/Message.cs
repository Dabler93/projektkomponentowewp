﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookTestin.Services.Model
{
    class Message
    {
        public DateTime UpdatedAt { get; set; }
        public String SenderId { get; set; }
        public String MessageText { get; set; }

    }
}
