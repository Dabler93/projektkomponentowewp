﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using mydateapplicationService.DataObjects;
using mydateapplicationService.Models;
using mydateapplicationService.Utils;

namespace mydateapplicationService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.Anonymous)]
    public class CustomRegistrationController : ApiController
    {
        public ApiServices Services { get; set; }

        // POST api/CustomRegistration
        public HttpResponseMessage Post(RegistrationRequest registrationRequest)
        {
            mydateapplicationContext context = new mydateapplicationContext();
            Account account = context.Accounts.Where(a => a.AccessToken == registrationRequest.access_token).SingleOrDefault();
            if (account != null)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, "User already registered");
            }
            else
            {
                Account newAccount = new Account
                {
                    Id = Guid.NewGuid().ToString(),
                    AccessToken = registrationRequest.access_token,
                    FirstName =  registrationRequest.firstName,
                    LastName = registrationRequest.lastName,
                    Gender = registrationRequest.gender,
                    Email = registrationRequest.email,
                    Picture = registrationRequest.picture,
                    Age = registrationRequest.age,
                    City = registrationRequest.city,
                    PreferredCity = registrationRequest.preferredCity,
                    PreferredAge = registrationRequest.preferredAge,
                    WomansPrefferedGender = registrationRequest.womansPrefferedGender
                };
                context.Accounts.Add(newAccount);
                context.SaveChanges();
                return this.Request.CreateResponse(HttpStatusCode.Created);
            }
        }
    }   
}
