﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using mydateapplicationService.DataObjects;
using mydateapplicationService.Models;
using mydateapplicationService.Utils;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace mydateapplicationService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.Anonymous)]
    public class RelationController : ApiController
    {
        public ApiServices Services { get; set; }
        public IServiceTokenHandler handler { get; set; }

        // POST api/CustomLogin
        public HttpResponseMessage Post(Boolean doLike, String sourceUser, String targetUser)
        {
            Boolean newFriends = false;
            using (mydateapplicationContext context = new mydateapplicationContext())
            {
                UsersRelation usersRelation = new UsersRelation();
                usersRelation.Id = Guid.NewGuid().ToString();
                usersRelation.SourceUser = sourceUser;
                usersRelation.TargetUser = targetUser;
                usersRelation.DoLike = doLike;
                context.UsersRelations.Add(usersRelation);
                if (doLike)
                {
                    var matchingUsersList = from relation in context.UsersRelations
                        where relation.SourceUser == targetUser
                              && relation.TargetUser == sourceUser
                        select relation.DoLike;
                    Boolean result = matchingUsersList.First();
                    if (result)
                    {
                        Account targetAccount = context.Accounts.Where(a => a.AccessToken.Equals(targetUser)).SingleOrDefault();
                        Contact friend = new Contact();
                        friend.Id = Guid.NewGuid().ToString();
                        friend.Source = sourceUser;
                        friend.Target = targetUser;
                        friend.TargetName = targetAccount.FirstName;
                        context.Contacts.Add(friend);
                        Contact reverse = new Contact();
                        Account sourceAccount = context.Accounts.Where(a => a.AccessToken.Equals(sourceUser)).SingleOrDefault();
                        reverse.Id = Guid.NewGuid().ToString();
                        reverse.Source = targetUser;
                        reverse.Target = sourceUser;
                        reverse.TargetName = sourceAccount.FirstName;
                        context.Contacts.Add(reverse);
                        newFriends = true;
                    }
                }
                context.SaveChanges();
            }
            if (newFriends)
            {
                return this.Request.CreateResponse(true);
            }
           return this.Request.CreateResponse(false);
        }
    }
}
