﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using mydateapplicationService.Models;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace mydateapplicationService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.Anonymous)]
    public class CustomChatController : ApiController
    {
        public ApiServices Services { get; set; }

        public IServiceTokenHandler handler { get; set; }

        // POST api/CustomLogin
        public HttpResponseMessage Post(String sourceUser, String targetUser)
        {
            mydateapplicationContext context = new mydateapplicationContext();
            IEnumerable matchingUsersList = from message in context.SentMessages
                                            where message.RecieverId == sourceUser
                                            && message.SenderId == targetUser
                                            select new { message.UpdatedAt, message.SenderId, message.Message };
            return this.Request.CreateResponse(matchingUsersList);
        }

    }
}
