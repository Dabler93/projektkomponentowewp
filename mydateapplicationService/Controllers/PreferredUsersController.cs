﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;
using mydateapplicationService.DataObjects;
using mydateapplicationService.Models;

namespace mydateapplicationService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.Anonymous)]
    public class PreferredUsersController : ApiController
    {
        public ApiServices Services { get; set; }
        public IServiceTokenHandler handler { get; set; }

        // POST api/CustomLogin
        public HttpResponseMessage Post(String user)
        {
            mydateapplicationContext context = new mydateapplicationContext();
            Account account = context.Accounts.Where(a => a.AccessToken.Equals(user)).SingleOrDefault();
            if (account != null)
            {
                IEnumerable matchingUsersList = from a in context.Accounts
//                                                from ur in context.UsersRelations.Where(x => x.SourceUser == account.AccessToken)
                    where a.AccessToken != account.AccessToken
                    && a.City.Equals(account.PreferredCity)
                    && a.Age.Equals(account.PreferredAge)
//                    && a.AccessToken != ur.TargetUser 
                    select new {a.FirstName, a.Gender, a.AccessToken, a.Age, a.Picture, a.City};
                
                return this.Request.CreateResponse(matchingUsersList);
            }
            return this.Request.CreateResponse(HttpStatusCode.Unauthorized, "Invalid username or password");
        }
    }
}
