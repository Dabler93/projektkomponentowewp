﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using mydateapplicationService.DataObjects;
using mydateapplicationService.Models;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Security;

namespace mydateapplicationService.Controllers
{
    [AuthorizeLevel(AuthorizationLevel.Anonymous)]
    public class CustomContactsController : ApiController
    {
        public ApiServices Services { get; set; }

        public IServiceTokenHandler handler { get; set; }

        // POST api/CustomLogin
        public HttpResponseMessage Post(String user)
        {
            mydateapplicationContext context = new mydateapplicationContext();
            IEnumerable matchingUsersList = from contact in context.Contacts
                                            where contact.Source == user
                                            select new { contact.Id, contact.Target, contact.TargetName };
            return this.Request.CreateResponse(matchingUsersList);
        }

    }
}
