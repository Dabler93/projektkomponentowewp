namespace mydateapplicationService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MojaMigracja : DbMigration
    {
        public override void Up()
        {
            AddColumn("mydateapplication.Messages", "SenderId", c => c.String());
            AddColumn("mydateapplication.Messages", "RecieverId", c => c.String());
            AddColumn("mydateapplication.Messages", "SentMessage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("mydateapplication.Messages", "SentMessage");
            DropColumn("mydateapplication.Messages", "RecieverId");
            DropColumn("mydateapplication.Messages", "SenderId");
        }
    }
}
