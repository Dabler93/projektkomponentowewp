﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Mobile.Service;

namespace mydateapplicationService.DataObjects
{
    public class SentMessage:EntityData
    {
        public String SenderId { get; set; }
        public String RecieverId { get; set; }
        public String Message { get; set; }
    }
}