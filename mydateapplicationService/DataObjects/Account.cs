﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Mobile.Service;

namespace mydateapplicationService.DataObjects
{
    public class Account : EntityData
    {
        public string AccessToken { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Gender { get; set; }
        public String Email { get; set; }
        public String Picture { get; set; }
        public int Age { get; set; }
        public String City { get; set; }
        public String PreferredCity { get; set; }
        public int PreferredAge { get; set; }
        public Boolean WomansPrefferedGender { get; set; }
    }
}