﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Mobile.Service;

namespace mydateapplicationService.DataObjects
{
    public class UsersRelation:EntityData
    {
        public String SourceUser { get; set; }
        public String TargetUser { get; set; }
        public Boolean DoLike { get; set; }
    }
}