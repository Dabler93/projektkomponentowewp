﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Mobile.Service;

namespace mydateapplicationService.DataObjects
{
    public class Contact:EntityData
    {
        public String Source { get; set; }
        public String Target { get; set; }
        public String TargetName { get; set; }
    }
}