﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mydateapplicationService.Models
{
    public class RegistrationRequest
    {
        public String access_token { get; set; }
        public String firstName { get; set; }
        public String lastName { get; set; }
        public String gender { get; set; }
        public String email { get; set; }
        public String picture { get; set; }
        public int age { get; set; }
        public String city { get; set; }
        public String preferredCity { get; set; }
        public int preferredAge { get; set; }
        public Boolean womansPrefferedGender { get; set; }
    }
}